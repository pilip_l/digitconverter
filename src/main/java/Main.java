import decimal.DigitalNumber;
import decimal.DigitalNumberFactory;
import decimal.NumberToWordConverter;

public class Main {

    public static void main(String[] args) {
        DigitalNumberFactory factory = new DigitalNumberFactory();
        DigitalNumber digitalNumber = factory.createInstance("123467890");
        NumberToWordConverter converter = new NumberToWordConverter();
        System.out.println(converter.convert(digitalNumber));
    }
}
