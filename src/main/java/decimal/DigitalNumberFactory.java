package decimal;

import java.util.LinkedList;

public class DigitalNumberFactory {
    public DigitalNumber createInstance(final String stringNumber) {
        StringBuilder number = new StringBuilder(stringNumber);
        int length = number.length();
        int remainder = length % 3;
        StringBuilder threeDigitNumber = new StringBuilder();
        while (remainder-- > 0) {
            threeDigitNumber.append(number.charAt(0));
            number.deleteCharAt(0);
        }
        LinkedList<ThreeDigitNumber> baseNumber = new LinkedList<>();
        if (threeDigitNumber.length() > 0) {
            baseNumber.push(new ThreeDigitNumber(threeDigitNumber.toString(), getPowerIndex(length)));
            length = number.length();
        }
        while (number.length() > 0) {
            threeDigitNumber = new StringBuilder();
            while (threeDigitNumber.length() < 3) {
                threeDigitNumber.append(number.charAt(0));
                number.deleteCharAt(0);
            }
            baseNumber.add(new ThreeDigitNumber(threeDigitNumber.toString(), getPowerIndex(length)));
            length = number.length();
        }
        return new DigitalNumber(baseNumber);
    }

    private int getPowerIndex(int length) {
        return (int) (Math.ceil((double) length / 3) - 1);
    }
}
