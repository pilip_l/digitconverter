package decimal;

import java.util.LinkedList;

public class DigitalNumber {
    private final LinkedList<ThreeDigitNumber> baseNumbers;

    DigitalNumber(LinkedList<ThreeDigitNumber> baseNumbers) {
        this.baseNumbers = baseNumbers;
    }

    LinkedList<ThreeDigitNumber> getBaseNumbers() {
        return baseNumbers;
    }
}
