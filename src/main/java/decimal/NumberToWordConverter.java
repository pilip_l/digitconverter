package decimal;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.logging.Logger;

public class NumberToWordConverter {
    private static final Map<Integer, String> shortScale;
    private static final Map<Integer, String> baseNames;
    private static final Logger log = Logger.getLogger(NumberToWordConverter.class.getName());
    private static final String NA = "на";
    private static final String E = "е";
    private static final String I = "и";
    private static final String A = "а";
    private static final String OV = "ов";
    private static final String SPACE = " ";

    static {
        shortScale = readProperties("short_scale.property");
        baseNames = readProperties("base_numbers.property");
    }

    private static Map<Integer, String> readProperties(final String fileName) {
        final Map<Integer, String> map = new HashMap<>();
        try (final InputStream inputStream = NumberToWordConverter.class.getClassLoader().getResourceAsStream(fileName);
             final InputStreamReader inputStreamReader =
                     new InputStreamReader(Objects.requireNonNull(inputStream), StandardCharsets.UTF_8.name())) {
            final Properties prop = new Properties();
            prop.load(inputStreamReader);
            prop.stringPropertyNames().forEach(number -> map.put(Integer.valueOf(number), prop.getProperty(number)));
        } catch (IOException e) {
            log.info("There is no such file as " + fileName);
        }
        return map;
    }

    public String convert(final DigitalNumber digitalNumber) {
        StringJoiner words = new StringJoiner(SPACE);
        for (ThreeDigitNumber threeDigitNumber : digitalNumber.getBaseNumbers()) {
            if (threeDigitNumber.getValue() != 0) {
                final List<Integer> baseNumbers = getBaseNumbers(threeDigitNumber.getValue());
                final String baseNumbersName = baseNumbersToNames(baseNumbers);
                final String powerName = powerIndexToName(threeDigitNumber.getPowerIndex());
                words.add(composeWords(baseNumbersName, powerName,
                        baseNumbers.get(baseNumbers.size() - 1), threeDigitNumber.getPowerIndex()));
            }
        }
        return words.toString().trim();
    }

    private String baseNumbersToNames(final List<Integer> baseNumbers) {
        final StringJoiner name = new StringJoiner(SPACE);
        baseNumbers.forEach(number -> name.add(baseNames.get(number)));
        return name.toString();
    }

    private String powerIndexToName(int powerIndex) {
        return shortScale.get(powerIndex);
    }

    private String composeWords(String baseNumbersName, String powerName, final int lastBaseNumber, final int powerIndex) {
        String words;
        if (lastBaseNumber == 1) {
            words = inflexionOfOne(new StringBuilder(baseNumbersName), new StringBuilder(powerName), powerIndex);
        } else if (lastBaseNumber == 2) {
            words = inflexionOfTwo(new StringBuilder(baseNumbersName), new StringBuilder(powerName), powerIndex);
        } else if (lastBaseNumber > 2 && lastBaseNumber < 5) {
            words = inflexionOfThreePerFour(new StringBuilder(baseNumbersName), new StringBuilder(powerName), powerIndex);
        } else {
            words = inflexionOfFiveAndOthers(new StringBuilder(baseNumbersName), new StringBuilder(powerName), powerIndex);
        }
        return words;
    }

    private String inflexionOfOne(StringBuilder baseNumbersName, StringBuilder powerName, final int powerIndex) {
        final int numbersLength = baseNumbersName.length();
        //thousand
        if (powerIndex == 1) {
            baseNumbersName.replace(numbersLength - NA.length(), numbersLength, NA);
        }
        return getWords(baseNumbersName, powerName);
    }

    private String inflexionOfTwo(StringBuilder baseNumbersName, StringBuilder powerName, final int powerIndex) {
        final int numbersLength = baseNumbersName.length();
        final int powerLength = powerName.length();
        //thousand
        if (powerIndex == 1) {
            baseNumbersName.replace(numbersLength - E.length(), numbersLength, E);
            powerName.replace(powerLength - I.length(), powerLength, I);
        }
        //million and others
        else if (powerIndex != 0) {
            powerName.append(A);
        }
        return getWords(baseNumbersName, powerName);
    }

    private String inflexionOfThreePerFour(StringBuilder baseNumbersName, StringBuilder powerName, final int powerIndex) {
        final int powerLength = powerName.length();
        //thousand
        if (powerIndex == 1) {
            powerName.replace(powerLength - I.length(), powerLength, I);
        }
        //million and others
        else if (powerIndex != 0) {
            powerName.replace(powerLength, powerLength, A);
        }
        return getWords(baseNumbersName, powerName);
    }

    private String inflexionOfFiveAndOthers(StringBuilder baseNumbersName, StringBuilder powerName, final int powerIndex) {
        final int powerLength = powerName.length();
        //thousand
        if (powerIndex == 1) {
            powerName.deleteCharAt(powerLength - 1);
        }
        //million and others
        else if (powerIndex != 0) {
            powerName.append(OV);
        }
        return getWords(baseNumbersName, powerName);
    }

    private String getWords(final StringBuilder baseNumbersName, final StringBuilder powerName) {
        return baseNumbersName.append(SPACE).append(powerName).toString();
    }

    private List<Integer> getBaseNumbers(int threeDigitNumber) {
        final int lastTwoDigits = threeDigitNumber % 100;
        //base multiplier for current digit
        int multiplier = 1;
        List<Integer> baseNumbers = new LinkedList<>();
        //expecting for number's last two digits be 10, 11, 12, ..., 19
        if (lastTwoDigits > 9 && lastTwoDigits < 20) {
            baseNumbers.add(lastTwoDigits);
            //remove last two digits of number
            threeDigitNumber /= 100;
            multiplier *= 100;
        }
        //for every last digit
        while (threeDigitNumber > 0) {
            int digit = threeDigitNumber % 10;
            if (digit != 0) {
                baseNumbers.add(digit * multiplier);
            }
            //remove current digit
            threeDigitNumber /= 10;
            multiplier *= 10;
        }
        Collections.reverse(baseNumbers);
        return baseNumbers;
    }
}
