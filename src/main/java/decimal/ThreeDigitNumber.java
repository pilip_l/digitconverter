package decimal;

class ThreeDigitNumber {
    private final int value;
    private final int powerIndex;

    private ThreeDigitNumber(final int value, final int powerIndex) {
        this.value = value;
        this.powerIndex = powerIndex;
    }

    ThreeDigitNumber(final String value, final int powerIndex) {
        this(Integer.parseInt(value), powerIndex);
    }

    int getValue() {
        return value;
    }

    int getPowerIndex() {
        return powerIndex;
    }
}
