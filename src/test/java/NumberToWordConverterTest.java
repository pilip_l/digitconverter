import decimal.DigitalNumber;
import decimal.DigitalNumberFactory;
import decimal.NumberToWordConverter;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

class NumberToWordConverterTest {

    @ParameterizedTest
    @CsvFileSource(resources = "data.csv", numLinesToSkip = 1)
    void testConvert(String input, String expected) {
        DigitalNumberFactory factory = new DigitalNumberFactory();
        DigitalNumber digitalNumber = factory.createInstance(input);
        NumberToWordConverter converter = new NumberToWordConverter();
        assertEquals(expected, converter.convert(digitalNumber));
    }
}